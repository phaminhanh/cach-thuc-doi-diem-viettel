Quý độc giả đã hiểu rõ chương trình CSKH Viettel Cộng Cộng là gì hay chưa? Cách thức tích điểm và đổi điểm ra sao? Hãy cùng tham khảo thông tin chi tiết có trong bài viết này:

Chương trình CSKH Viettel Cộng Cộng là gì?
Viettel Cộng Cộng là chương trình CSKH Viettel đặc biệt dành riêng cho những khách hàng thân thiết từ nhà mạng Viettel. Khách hàng khi sử dụng các dịch vụ từ di động, truyền hình hay internet sẽ đều được quy đổi ra các điểm tích lũy. Dựa vào số lượng điểm tích lũy Viettel, 70 triệu khách hàng của nhà mạng sẽ được xếp hạng hội viên tương ứng và được hưởng những quyền lợi, ưu đãi đặc biệt. Ngoài ra, khách hàng sẽ hoàn toàn có thể đổi điểm tích lũy sang cước di động, phút gọi, data, sms, voucher dịch vụ.

Các hướng tích điểm Viettel Cộng Cộng
Tính Điểm Viettel Từ Cước Tiêu Dùng: 
Điểm được tính từ số tiền thực tế khách hàng thanh toán cước hoặc nạp thẻ. Định mức tích điểm 0.5% số tiền khách hàng thanh toán.
Xem thêm: Hướng dẫn đổi điểm Viettel: https://linkhay.com/link/2906531/cach-doi-diem-viettel-de-nhan-tien-qua-va-luu-luong-data

Tính điểm Viettel từ thời gian sử dụng: Số điểm cộng tính theo thời gian sử dụng của mỗi thuê bao: Từ 1- dưới 3 năm sẽ được + 1000 điểm, 3 – dưới 5 năm + 3000 điểm và từ 5 năm trở đi sẽ được + 5000 điểm

Tính điểm Viettel nâng cấp gói data: Nếu khách hàng nâng cấp gói Data tháng có giá trị cao hơn gói cũ trong vòng 30 ngày gần nhất: tích 5% giá trị gói Data được nâng cấp. Ngoài ra, khi khách hàng đăng ký gói các gói Data MI5D, ST15K, ST30K tích 5% giá trị gói Data đăng ký.

Tính điểm Viettel khi đăng ký thêm dịch vụ cố định: Khách hàng đang dùng dịch vụ di động đăng ký thêm dịch vụ cố định, sẽ được tích 5% giá trị gói cước cố định khách hàng đăng ký.

Tính điểm thưởng Viettel sinh nhật: Khách hàng hạng Kim Cương, Vàng, Bạc vào ngày sinh nhật sẽ được cộng điểm lần lượt là 500.000 điểm, 300.000 điểm và 100.000 điểm .

Tính điểm thưởng Viettel theo chiến dịch: Định mức cộng điểm cho khách hàng sẽ tùy thuộc vào từng chương trình của nhà mạng Viettel cụ thể.

Những Cách Thức Đổi Điểm Viettel Cộng Cộng

Để có thể đổi được điểm chương trình Viettel Cộng Cộng sang lưu lượng data, phút gọi, sms hay các voucher dịch vụ, khách hàng hãy thực hiện theo từng cách thức sau:
Xem thêm: https://community.constantcontact.com/t5/user/viewprofilepage/user-id/1122131
Đổi điểm Viettel sang cước dịch vụ:

Để đổi điểm Viettel sang cước dịch vụ, bạn hãy chọn thực hiện 1 trong 3 cách sau:

Cách 1: Tải và sử dụng trên ứng dụng My Viettel.
Cách 2: Thao tác qua USSD *098#.
Cách 3: Gọi tổng đài 18008098.
Đổi điểm Viettel sang lưu lượng Thoại/SMS/Data:

Để có thể đổi điểm Viettel sang lưu lượng data, đổi lưu lượng sms hay đổi điểm sang lưu lượng thoại bạn hãy thực hiện 1 trong 3 cách sau:

Cách 1: Tải và sử dụng trên ứng dụng My Viettel.
Cách 2: Thao tác qua USSD *098#.
Cách 3: Gọi tổng đài CSKH 18008098.
Đổi điểm Viettel sang gói dịch vụ:

Cách 1: Tải và sử dụng trên ứng dụng My Viettel hoặc thông qua website cong.viettel.vn.
Cách 2: Thao tác qua USSD *098#.
Đổi điểm tích lũy sang voucher đối tác liên kết:

Để có thể đổi điểm Viettel Cộng Cộng sang voucher của đối tác liên kết, khách hàng tiến hành tải và sử dụng trên ứng dụng My Viettel hoặc thông qua website: cong.viettel.vn. Các đối tác liên kết của nhà mạng Viettel hiện nay rất đa dạng trong mọi lĩnh vực nên khách hàng có thể thoải mái lựa chọn đổi nhiều loại voucher ưu đãi khác nhau.

Hy vọng rằng với những thông tin trên mà chúng tôi vừa mới chia sẻ sẽ giúp quý độc giả có thể hiểu rõ hơn về cách thức đổi điểm tích lũy Viettel. Mọi thắc mắc cần giải đáp về chương trình Viettel Cộng Cộng này quý bạn đọc hãy liên hệ với tổng đài CSKH của Viettel 1800.8098 nhé.
Nguồn: https://www.xmioviettel.net/nhung-cach-thuc-doi-diem-viettel-sang-uu-dai